

/*
	RESPONSIVE BS CAROUSEL v 2.0
	Agora para ter o seu bootstrap carousel responsivo basta add a classe 'carousel-responsive' e add nos attrs
	'data-md', 'data-sm' e 'data-xs' , a quantidade de itens que vc queira mostrar para tal midia:

	Exemplo:
		Um carousel com  4 itens no desktop, 3 no tablet e 1 no mobile

		<div class="carousel carousel-responsive">
			<div class="carousel-inner">

				<div class="col-md-3 col-sm-4"> ... </div>
				<div class="col-md-3 col-sm-4"> ... </div>
				<div class="col-md-3 col-sm-4"> ... </div>
				<div class="col-md-3 col-sm-4"> ... </div>

			</div>
		</div>
	OBS:
		- Os attrs 'data-md', 'data-sm' e 'data-xs' carregam consigo como valor default 1.
		- É OBRIGATÓRIO a atribuição de um 'id' para o carousel, caso o constrário, o mesmo não funcionará.
*/
$('.carousel-responsive').each(function(index, el) {
	var alvo = $('#'+$(this).attr('id'));
	var items = alvo.find('.carousel-inner > *');
	var responsive = {
		'xs': $(this).data('xs') || 1,
		'sm': $(this).data('sm') || 1,
		'md': $(this).data('md') || 1
	};
	var midia = 'xs';

	if($(window).width() > 700){
		midia = 'sm';
	}

	if($(window).width() > 991){
		midia = 'md';
	}

	function wrapCarousel(count){
		alvo.find('.carousel-inner .item > *').unwrap('<div class="item"></div>');

		for(i=0;i<items.length;i++){
			alvo.find('.carousel-inner > *').slice(i, i+count).wrapAll('<div class="item"></div>');
		}

		alvo.find('.item:first-child').addClass('active');
	}

	function refreshCarousel (){
		switch(midia){
			case 'xs':
				wrapCarousel(responsive[midia]);
			break;
			case 'sm':
				wrapCarousel(responsive[midia]);
			break;
			case 'md':
				wrapCarousel(responsive[midia]);
			break;
		}
	}

	refreshCarousel ();

	$(window).resize(function(event) {
		refreshCarousel ();
	});
});

$('.carousel[data-interval]').each(function(index, el) {
	$(this).carousel({
		interval: $(this).data('interval')
	});
});

$('a[data-carousel="prev"]').click(function(event) {
	event.preventDefault();

	$($(this).attr('href')).carousel('prev');
});

$('a[data-carousel="next"]').click(function(event) {
	event.preventDefault();

	$($(this).attr('href')).carousel('next');
});

$('.carousel').on('swipeleft',function(){
	$(this).carousel('next');
});

$('.carousel').on('swiperight',function(){
	$(this).carousel('prev');
});

var lazyBg = (function(){
	var isLazedBg = function(){
		$('[data-lazy-bg]').each(function(index, el) {
			if(el.getBoundingClientRect().top < window.innerHeight + 200){
				$(this).css('background-image', 'url('+$(this).data('lazy-bg')+')');
			}
		});
	};
	var jaLazyBg = false
	setTimeout(isLazedBg(),200);

	$(window).scroll(function(){
		if(jaLazyBg) return;
		setTimeout(function(){
			jaLazyBg =false;
		},100);

		isLazedBg();
	});
})();
var lazyImage = (function(){
	var isLazyPoint = function(){
		$('lazyimage').each(function(index, el) {
			if(el.getBoundingClientRect().top < window.innerHeight + 200){
				var classe = $(this).data('class') ? 'class="'+$(this).data('class')+'"' : '';
				$(this).html('<img src="'+$(this).data('src')+'" alt="'+$(this).data('alt')+'" '+classe+'/>').css('backgroundImage', 'none');
			}
		});
	};
	var jaLazyImage = false;

	setTimeout(isLazyPoint(),200);

	$(window).scroll(function(){
		if(jaLazyImage) return;

		setTimeout(function(){
			jaLazyImage = false
		},100);

		isLazyPoint();
	});
})();
function message_IE(){
	var msg = '<div id="msg-ie" class="msg-ie">'+
		'<div class="alert-danger alert text-center">'+
			'<button onclick="remover(\'#msg-ie\')" class="close"><i class="fa fa-times"></i></button>'+
			'<h1>ATEN&Ccedil;&Atilde;O!!</h1>'+
			'<p>O seu navegador est&aacute; desatualizado, para melhor funcionamento do site clique <a href="#" class="alert-link">aqui</a> para atulizar! Ou instale o <a href="#" class="alert-link">Google Chrome</a></p>'+
		'</div>'+
	'</div>';

	$('noscript').after(msg);
	$('body').addClass('ie');
}

function remover($target){
	$($target).fadeOut('fast',function(){
		$($target).remove();
	})
}

$('.pergunta').click(function(){
	$(this).toggleClass('text-success');
})
$('.carousel-servicos').owlCarousel({
	loop: true,
	autoplay:true,
	nav: true,
	navText: [
		'<i class="icone servico-prev"></i>',
		'<i class="icone servico-next"></i>'
	],
	responsive: {
		0: {
			items: 1,
		},
		600:{
			items : 1
		},
		992: {
			items: 1
		}
	}
});

$('#depoimentos').owlCarousel({
	nav: true,
	navText: [
		'<i class="icone servico-prev"></i>',
		'<i class="icone servico-next"></i>'
	],
	responsive: {
		0: {
			items: 1,
		},
		600:{
			items : 1
		},
		992: {
			items: 1
		}
	}
});

$('#unidades').owlCarousel({
	nav: true,
	navText: [
		'<i class="fa fa-chevron-left"></i>',
		'<i class="fa fa-chevron-right"></i>'
	],
	responsive: {
		0: {
			items: 1,
		},
		600:{
			items : 1
		},
		992: {
			items: 1
		}
	}
});
