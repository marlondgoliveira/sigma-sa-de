$('.carousel-servicos').owlCarousel({
	loop: true,
	autoplay:true,
	nav: true,
	navText: [
		'<i class="icone servico-prev"></i>',
		'<i class="icone servico-next"></i>'
	],
	responsive: {
		0: {
			items: 1,
		},
		600:{
			items : 1
		},
		992: {
			items: 1
		}
	}
});

$('#depoimentos').owlCarousel({
	nav: true,
	navText: [
		'<i class="icone servico-prev"></i>',
		'<i class="icone servico-next"></i>'
	],
	responsive: {
		0: {
			items: 1,
		},
		600:{
			items : 1
		},
		992: {
			items: 1
		}
	}
});

$('#unidades').owlCarousel({
	nav: true,
	navText: [
		'<i class="fa fa-chevron-left"></i>',
		'<i class="fa fa-chevron-right"></i>'
	],
	responsive: {
		0: {
			items: 1,
		},
		600:{
			items : 1
		},
		992: {
			items: 1
		}
	}
});
